function New-TrackGpoTicket_External {
    <#
    .SYNOPSIS
    This is a sample external function for use with the TrackGpo module. It only outputs details to console.

    .DESCRIPTION
    When the TrackGpo module detects any GPO changes, it will call ALL modules who contain a function with this same name.

    It is up to you to make a function that emails or calls an API or whatever you want GPO changes to land to.

    If you only create a function and don't do it as part of a module,
    ensure that your function clobbers the namespace for that function.
    Meaning, make sure that running the command `New-TrackGpoTicket_External`
    will run your function and not any other module version instead.
    Remember, the LAST one to define the function is the one tha clobbers.

    .PARAMETER Type
    What type of GPO change has happened

    .PARAMETER GpoInfo
    A hashtable with the following properties:
        Title
        Created
        Modified
        GUID
        'GPO Status'
        'Enabled Links'

    .PARAMETER Diff
    This is only included when there are changes from one version of a GPO to another and contains the diff-style changes
    Ex:
    TODO

    .PARAMETER Stats
    A string of details about the changes

    .EXAMPLE
    New-TrackGpoTicket_External -Type Add -GpoInfo $GpoInfo

    .EXAMPLE
    New-TrackGpoTicket_External -Type Change -GpoInfo $GpoInfo -Diff $Diff

    .NOTES
    You may need to remove any non-ascii characters if your ticketing software sucks:
        | ForEach-Object {
        [system.Text.Encoding]::ASCII.Getstring((
                [system.Text.Encoding]::Default.GetBytes(($PSItem))
            ))
        }

    You may need to indent each line in your GpoInfo object with 4 spaces for your ticketing software to see it as code:
        ($GpoInfo | Format-Table -AutoSize | Out-String) -replace "(?m)^", "    " -replace "\r"
    #>
    [CmdletBinding(SupportsShouldProcess)]
    param(
        [ValidateSet("Add", "Remove", "Change")]
        [parameter(Mandatory)]$Type,
        [parameter(Mandatory)]$GpoInfo,
        $Diff,
        $Stats
    )
    if ($pscmdlet.ShouldProcess($GpoInfo.Title, "Notify about GPO change")) {
        switch ($Type) {
            "Add" {
                "New GPO Added! {0}" -f $GpoInfo.Title | Out-Host
                $GpoInfo | Out-Host
            }
            "Remove" {
                "GPO Removed! {0}" -f $GpoInfo.Title | Out-Host
                $GpoInfo | Out-Host
            }
            "Change" {
                "GPO Edited! {0}" -f $GpoInfo.Title | Out-Host
                $GpoInfo | Out-Host
                $Stats | Out-Host
                $Diff | Out-Host
            }
        }
        $CommitMessage = "This is a commit message for {0}: {1}" -f $GpoInfo.Title, (Get-Date)
        # The output of this function will get saved as the Git commit message if you didn't disable Git.
        $CommitMessage
    }
}

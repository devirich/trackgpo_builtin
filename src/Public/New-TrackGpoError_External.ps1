function New-TrackGpoError_External {
    [CmdletBinding()]
    param(
        [parameter(Mandatory)]
        [String]$Message
    )

    Write-Information "You could email a ticket, or hit an API with this error:"
    Write-Information $Message
}
